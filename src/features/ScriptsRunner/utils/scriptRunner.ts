interface IScriptRunnerProps {
  [key: string]: number[];
}

interface IScriptRunnerResult {
  scripts: number[];
  nonExistentScripts: number[];
  errorMessage?: string;
  isCyclic: boolean,
  isContainingNonExistentScripts: boolean,
}

const useScriptRunner = (scriptsDatabase: IScriptRunnerProps): IScriptRunnerResult => {
  const visited: { [key: string]: boolean } = {};
  const inStack: { [key: string]: boolean } = {};
  const stack: number[] = [];
  const nonExistentScripts: number[] = [];
  let errorMessage = '';

  let isCyclic = false;
  let isContainingNonExistentScripts = false;

  const hasCycle = (scriptId: number): boolean => {
    if (inStack[scriptId]) {
      return true;
    }
    if (visited[scriptId]) {
      return false;
    }
    if (!Object.keys(scriptsDatabase).includes(String(scriptId))) {
      isContainingNonExistentScripts = true;
      nonExistentScripts.push(scriptId);
      errorMessage = `There are links to non-existent scripts: ${nonExistentScripts}`;
      return false;
    }

    visited[scriptId] = true;
    inStack[scriptId] = true;

    for (const dependency of Object.values(scriptsDatabase[scriptId])) {
      if (hasCycle(dependency)) {
        return true;
      }
    }
    stack.push(Number(scriptId));
    inStack[scriptId] = false;
    return false;
  };

  for (const scriptId of Object.keys(scriptsDatabase)) {
    if (hasCycle(Number(scriptId))) {
      isCyclic = true;
      errorMessage = 'The script database is cyclical, it is impossible to perform topological sorting';
    }
  }
  return {
    scripts: stack.reverse(),
    nonExistentScripts,
    errorMessage,
    isCyclic,
    isContainingNonExistentScripts,
  };
};

export { useScriptRunner };
export type { IScriptRunnerProps, IScriptRunnerResult };
