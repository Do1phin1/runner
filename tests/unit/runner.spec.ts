import { useScriptRunner } from '@/features/ScriptsRunner/utils/scriptRunner';

describe('Topological Sort', () => {
  test('should be topologically sorted', () => {
    const scriptsDatabase = {
      1: [2, 3],
      2: [4],
      3: [],
      4: [3],
      5: [1, 4],
      6: [],
    };

    const { scripts: result } = useScriptRunner(scriptsDatabase);
    expect(result).toEqual([6, 5, 1, 2, 4, 3]);
  });
  test('should be cyclic', () => {
    const scriptsDatabase = {
      1: [2],
      2: [3],
      3: [1],
    };

    const { scripts: result } = useScriptRunner(scriptsDatabase);
    expect(result).toHaveLength(0);
  });
  test('should be present only existing scripts', () => {
    const scriptsDatabase = {
      1: [2],
      2: [3],
      3: [4],
    };

    const { nonExistentScripts: result } = useScriptRunner(scriptsDatabase);
    expect(result.length).toBeGreaterThan(0);
  });
});
